use std::error::Error;
use std::fmt;

use quick_xml::encoding::EncodingError;
use quick_xml::events::attributes::AttrError;

#[derive(Debug)]
pub struct PlaylistDecodeError {
    details: String,
}

impl PlaylistDecodeError {
    pub fn new(msg: &str) -> PlaylistDecodeError {
        PlaylistDecodeError {
            details: msg.to_string(),
        }
    }
}

impl fmt::Display for PlaylistDecodeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "DecodeError: {}", self.details)
    }
}

impl Error for PlaylistDecodeError {}

impl From<AttrError> for PlaylistDecodeError {
    fn from(_value: AttrError) -> Self {
        PlaylistDecodeError {
            details: String::from("XML: AttrError"),
        }
    }
}

impl From<EncodingError> for PlaylistDecodeError {
    fn from(_value: EncodingError) -> Self {
        PlaylistDecodeError {
            details: String::from("XML: EncodingError"),
        }
    }
}

impl From<quick_xml::Error> for PlaylistDecodeError {
    fn from(_value: quick_xml::Error) -> Self {
        PlaylistDecodeError {
            details: String::from("XML: generic error"),
        }
    }
}
